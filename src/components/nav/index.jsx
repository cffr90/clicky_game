import React from 'react'
import PropTypes from 'prop-types'

export default function Nav({ totalScore }) {
  return (
    <header className='container'>
      <div className='d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm'>
        <h5 className='my-0 mr-md-auto font-weight-normal'>Clicky Game</h5>
        <nav className='my-2 my-md-0 mr-md-3'>
          <a className='p-2 text-dark'>Click an image to begin!</a>
          <a className='p-2 text-dark'>
            Score:{totalScore.score} | Top Score:
            {totalScore.topScore}
          </a>
        </nav>
      </div>
    </header>
  )
}

Nav.propTypes = {
  totalScore: PropTypes.number.isRequired
}
