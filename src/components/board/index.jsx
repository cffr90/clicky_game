import React from 'react'
import PropTypes from 'prop-types'

import Card from '../card'

export default function Board({
  solved,
  disabled,
  cards,
  flipped,
  handleClick
}) {
  return (
    <div className='row'>
      {cards.map(card => (
        <Card
          key={card.id}
          width={100}
          height={100}
          handleClick={handleClick}
          flipped={flipped.includes(card.id)}
          solved={solved.includes(card.id)}
          disabled={disabled || solved.includes(card.id)}
          {...card}
        />
      ))}
    </div>
  )
}

Board.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  flipped: PropTypes.arrayOf(PropTypes.number).isRequired,
  handleClick: PropTypes.func.isRequired,
  solved: PropTypes.arrayOf(PropTypes.number).isRequired,
  disabled: PropTypes.bool.isRequired
}
