import React from 'react'
import PropTypes from 'prop-types'

export default function Card({
  handleClick,
  flipped,
  id,
  height,
  type,
  width,
  disabled,
  solved
}) {
  return (
    <div
      className={`flip-container ${flipped ? 'flipped' : ''} col-md-3 my-2`}
      style={{
        width,
        height
      }}
      onClick={() => (disabled ? null : handleClick(id))}>
      <div className='flipper text-center'>
        <img
          alt={type}
          className={flipped ? 'front' : 'back'}
          src={
            flipped || solved
              ? `/assets/images/${type}.jpg`
              : '/assets/images/back.jpeg'
          }
          style={{ width, height }}
        />
      </div>
    </div>
  )
}

Card.propTypes = {
  solved: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  flipped: PropTypes.bool.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  handleClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired
}
