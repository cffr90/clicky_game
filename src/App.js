import React, { useState, useEffect } from 'react'
import Board from './components/board'
import deck from './deck'

export default function App() {
  const [cards, setCards] = useState([]) //initial var @ empty arr
  const [flipped, setFlipped] = useState([]) //initial var @ empty arr
  const [topScore, setTopScore] = useState([0]) //initial var @ empty arr
  const [score, setScore] = useState(0) //initial var @ 0
  const [solved, setSolved] = useState([])
  const [disabled, setDisabled] = useState(false)

  useEffect(() => {
    setCards(deck()) //set arr with random cards
    // console.log(deck())
  }, [])

  const sameCardClickedTwice = id => flipped.includes(id)
  const isAMatch = id => {
    const clickedCard = cards.find(card => card.id === id)
    const flippedCard = cards.find(card => flipped[0] === card.id)
    return flippedCard.type === clickedCard.type
  }
  const handleClick = id => {
    // all the magic happens here
    setDisabled(true)
    if (flipped.length === 0) {
      setFlipped(flipped => [...flipped, id])
      setDisabled(false)
    } else {
      if (sameCardClickedTwice(flipped, id)) {
        return setFlipped(flipped => [...flipped, id])
      }
      if (isAMatch(id)) {
        setSolved([...solved, ...flipped, id])
        setFlipped([])
        setDisabled(false)
        setScore(() => {
          return score + 1
        })
        setTopScore(topScore => [...topScore, score + 1])
      }
      //flicpback
      else {
        alert('BAD GUESS!!!')
        setScore(0)
        setFlipped([])
        setDisabled(false)
        // setCards(deck())
      }
    }
  }

  return (
    <div>
      <header className='container'>
        <div className='d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm'>
          <h5 className='my-0 mr-md-auto font-weight-normal'>Clicky Game</h5>
          <nav className='my-2 my-md-0 mr-md-3'>
            <a className='p-2 text-dark'>Click an image to begin!</a>
            <a className='p-2 text-dark'>
              Score:{score} | Top Score:{Math.max(...topScore)}
            </a>
          </nav>
        </div>
      </header>
      <div className='container text-center'>
        <div className='my-5 '>
          <h1>Clicky Game</h1>
          <h5>Start Clicking now to guess !!! </h5>
        </div>
      </div>

      <Board
        disabled={disabled}
        cards={cards}
        solved={solved}
        flipped={flipped}
        handleClick={handleClick}
      />
    </div>
  )
}
